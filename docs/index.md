# Bienvenue

Nous avons créé ce site pour partager nos apprentissages informatiques en
famille !

L'idée est d'en faire un peu tous les jours, et surtout de le documenter.

Nous espérons que ça puisse également servir à d'autres, n'hésitez pas à
contribuer si vous souhaitez clarifier un contenu ou corriger des erreurs.

## Qui sommes nous ?

![ach](./images/avatars/ach-200.png){ width="100px" }
![nch](./images/avatars/nch-200.png){ width="100px" }
![mch](./images/avatars/mch-200.png){ width="100px" }

- Alexandre, je travaille dans l'informatique depuis environ 20 ans, passionné
  depuis bien plus longtemps. J'ai deux entreprises dans le domaine.

- Mes enfants, souhaitant se diriger à ce jour dans l'informatique plus tard,
  c'est l'occasion de découvrir des sujets pour apprendre de manière ludique !

## Nos objectifs

### Pour Alexandre

- c'est l'occasion de revoir et redécouvrir des sujets vus il y a longtemps
- d'actualiser mes connaissances et de les formaliser
- de découvrir de nouvelles choses, en famille, et de les partager avec mes
  enfants.

### Pour les enfants

- découvrir comment utiliser l'outil informatique
- savoir ce qu'il se passe réellement
- leur donner les moyens d'aller vers ce qu'ils veulent faire plus tard
  (créateur de jeux vidéos pour l'un et informaticien "généraliste" pour
  l'autre)
