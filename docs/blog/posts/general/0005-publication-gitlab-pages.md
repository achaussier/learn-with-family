---

categories:
  - Git
  - Gitlab
  - Mkdocs
date: 2024-06-28

---

# Publication du site sur Gitlab Pages

Grand jour, nous avons publié notre site pour qu'il soit accessible directement
et publiquement sur Gitlab Pages.

Nous avons rajouté une section pour décrire les étapes à réaliser [sur cette page](../../../documentation/mkdocs/premiers-pas/#deployer-sur-le-service-gitlab-pages).

Vous pouvez voir le résultat [sur cette page](https://learn-with-family-achaussier-96ca1d7dc60c160546367b5c8947347f39.gitlab.io/).

A prévoir pour la suite, changer le nom et l'adresse du site !
