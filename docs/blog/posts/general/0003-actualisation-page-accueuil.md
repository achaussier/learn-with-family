---

categories:
  - General
date: 2024-06-26

---

# Remplissage de la page d'accueil

Peu de temps aujourd'hui, nous avons donc rempli la page d’accueil.

Vous pouvez la consulter [directement ici](../../../index.md).

Un résumé de notre contenu :

- création de nos avatars. Nous avons utilisé le service de [Xavatar.io](https://fr.xavatar.io/#generateur).
- présentation rapide de notre projet et de nous-même.
- présentation de nos objectifs.
