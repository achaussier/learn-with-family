---

categories:
  - General
date: 2024-06-23

---

# Lancement du site

Après en avoir parlé avec mes enfants qui ont eu également envie de
m'accompagner, nous avons donc préparé la base de notre site qui référencera
tous nos apprentissages.

Ce site a été créé à l'aide de [MKDocs](https://www.mkdocs.org/) et du thème
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).
