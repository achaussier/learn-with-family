---

categories:
  - General
  - Mkdocs
date: 2024-06-25

---

# Premiers pas avec MKDocs

Nous avons créé le site que nous allons publier sur [Gitlab pages](https://gitlab.com/).

Les enfants ont pu voir comment:

- créer un site à l'aide de [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).
- créer un article en [Markdown](https://www.markdownguide.org/) pour documenter
  nos apprentissages.
- créer un article pour constituer un blog.

On a également commencé configurer MKDocs en mettant en place le mode sombre.

Vous pouvez retrouver le rendu associé à cette journée [sur cette page](../../../documentation/mkdocs/premiers-pas.md).
