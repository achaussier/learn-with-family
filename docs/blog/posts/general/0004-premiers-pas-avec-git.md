---

categories:
  - Git
  - Gitlab
date: 2024-06-27

---

# Premiers pas avec Git et Gitlab

Nous avons mis en place [Git](https://git-scm.com/) pour le projet et l'avons
poussé sur [Gitlab](https://gitlab.com/achaussier/learn-with-family).

Nous préparerons une page dédiée sur Git d'ici quelques jours pour l'expliquer
et référencer les commandes.

Pour le moment, nous avons créé le contenu suivant:

- [création du dépôt local Git](../../../documentation/mkdocs/premiers-pas.md#creation-du-depot-git).
- [étapes pour créer un projet sur Gitlab](../../../git/gitlab/creer-projet-git.md).
