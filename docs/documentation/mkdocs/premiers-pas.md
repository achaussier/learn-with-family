# Premiers pas avec MKDocs

[MKdocs](https://www.mkdocs.org/) est un outil de documentation qui utilise la
syntaxe Markdown pour générer un site statique de documentation.

## Installation

1. Créer un environnement virtuel et le charger

    ```bash
    python3 -m venv .venv
    source .venv/bin/activate
    ```

2. Référencer la dépendance `mkdocs` à installer dans le fichier
   `requirements.txt`

    ```bash
    echo "mkdocs==1.6.0" >> requirements.txt
    ```

3. Installer les dépendances

    ```bash
    pip install -r requirements.txt
    ```

4. Créer un nouveau projet dans le dossier courant.

    ```bash
    mkdocs new .
    ```

    Cela creera l'arborescence suivante :

    ```bash
    .
    ├── docs
    │   └── index.md
    └── mkdocs.yml
    ```

5. Lancer le serveur et valider en allant sur l'URL
   [http://localhost:8000](http://localhost:8000)

    ```bash
    mkdocs serve
    ```

## Configuration

Il est possible de configurer le comportement et le rendu du site généré en
rajoutant du code dans le fichier `mkdocs.yml`.

### Modifier le nom du site

Par exemple, pour modifier le nom du site, on va modifier la valeur de la clé
`site_name` dans le fichier `mkdocs.yml`.

```yaml
---
site_name: Mon site
```

### Modifier le thème

Pour que le site soit plus complet, on va utiliser le thème [material](https://squidfunk.github.io/mkdocs-material/).

1. Ajouter le thème comme dépendance dans le fichier `requirements.txt`.

    ```bash
    echo "mkdocs-material==9.5.27" >> requirements.txt
    ```

2. Installer les nouvelles dépendances.

    ```bash
    pip install -r requirements.txt
    ```

3. Ajouter le thème dans le fichier `mkdocs.yml`.

    ```yaml
    theme:
      name: material
    ```

### Intégrer le mode sombre

Il est possible d'intégrer le mode sombre dans le thème **material**.

Il faut indiquer les palettes de couleurs dans la configuration du thème dans le
fichier `mkdocs.yml`.

```yaml
theme:
  name: material
  palette:
    # Palette toggle for light mode
    - scheme: default
      toggle:
        icon: material/brightness-7 # (2)!
        name: Switch to dark mode

    # Palette toggle for dark mode
    - scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
```

## Publication

### Création du dépot Git

1. Créer un dépôt Git pour le projet.

    ```bash
    git init
    ```

2. Créer un commit vide pour initialiser le projet.

    ```bash
    git commit --allow-empty -m 'build: initialize repository'
    ```

3. Créer un fichier `.gitignore` pour ignorer le dossier de l'environnement
   virtuel.

    ```bash title=".gitignore"
    # Python
    .venv/

    # Vim
    *.swo
    *.swp
    ```

4. Ajouter le contenu déjà créé.

    ```bash
    git add .gitignore docs mkdocs.yml requirements.txt
    ```

5. Créer un commit.

    ```bash
    git commit -m 'docs: initialize content with mkdocs basics'
    ```

### Liaison avec Gitlab

Nous allons publier le site sur [Gitlab](https://gitlab.com/).

1. [Créer un projet Gitlab](../../git/gitlab/creer-projet-git.md) avec le nom de votre projet.

2. Lier le dépôt local au projet Gitlab.

    ```bash
    git remote add origin git@gitlab.com:achaussier/learn-with-family.git
    ```

3. Envoyer le contenu local sur le projet Gitlab.

    ```bash
    git push origin main
    ```

### Déployer sur le service Gitlab pages

1. Créer un fichier `.gitlab-ci.yml` avec le contenu suivant:

    ```yaml title=".gitlab-ci.yml"
    image: python:3.12-alpine

    before_script:
      - pip install -r requirements.txt

    test:
      stage: test
      script:
      - mkdocs build --strict --verbose --site-dir public
      artifacts:
        paths:
        - public
      rules:
        - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

    pages:
      stage: deploy
      script:
      - mkdocs build --strict --verbose --site-dir public
      artifacts:
        paths:
        - public
      rules:
        - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
    ```

2. Créer un commit avec ce nouveau contenu.

    ```bash
    git add .gitlab-ci.yml
    git commit -m 'ci: add GitlabCI configuration and documentation'
    ```
