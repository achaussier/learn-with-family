# Créer un projet Git sur Gitlab

1. Aller sur [Gitlab](https://gitlab.com/).

2. Cliquer sur le bouton `+`.

    ![menu01](../../images/git/gitlab/creer-projet-git/menu-01.png){ width="300" }

3. Cliquer sur **Create blank project**.

    ![menu02](../../images/git/gitlab/creer-projet-git/menu-02.png){ width="300" }

4. Remplir le formulaire.

    ![form](../../images/git/gitlab/creer-projet-git/create-form.png){ width="400" }

